package colorfondo;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.Color;
import java.awt.Container;
import java.awt.FlowLayout;

public class ColorFondo extends JFrame implements ActionListener {

    private JMenuBar menu;
    private JMenu menu1;
    private JMenuItem mRojo,mVerde,mAzul;
    public ColorFondo() {
        
        menu=new JMenuBar();
        setJMenuBar(menu);
        menu1=new JMenu("Opciones");
        menu.add(menu1);
        
        mRojo=new JMenuItem("Rojo");
        mRojo.addActionListener(this);
        menu1.add(mRojo);
        
        mVerde=new JMenuItem("Verde");
        mVerde.addActionListener(this);
        menu1.add(mVerde);
        
        mAzul=new JMenuItem("Azul");
        mAzul.addActionListener(this);
        menu1.add(mAzul);               
    }
    
    public void actionPerformed(ActionEvent e) {
    	Container f=this.getContentPane();
        if (e.getSource()==mRojo) {
            f.setBackground(Color.red);
        }
        if (e.getSource()==mVerde) {
            f.setBackground(Color.green);
        }
        if (e.getSource()==mAzul) {
            f.setBackground(Color.blue);
        }     
    }
    
    public static void main(String[] args) {
        ColorFondo fondo=new ColorFondo();
        fondo.setSize(400, 600);
        fondo.setVisible(true);
        fondo.setLayout(new FlowLayout());
        fondo.setLocationRelativeTo(null);
        fondo.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }
    
}
